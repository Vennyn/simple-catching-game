#include "FlowerManager.h"

FlowerManager::FlowerManager()
{
	loadTexture();
	this->generateFlowerPosition();
	std::cout << "done";
}

void FlowerManager::loadTexture()
{
	if (!t_red_flower.loadFromFile("img/red_flower.png"))throw std::runtime_error("Red flower failed to load");
	if (!t_blue_flower.loadFromFile("img/blue_flower.png"))throw std::runtime_error("Blue flower failed to load");
}

int FlowerManager::GetRandom(int x, int y)	//mabe change to random device
{
	static std::default_random_engine generator(std::clock());
	std::uniform_int_distribution<int> random_number(x, y);	
	return random_number(generator);
}

void FlowerManager::generateFlowerPosition()
{
	sf::Vector2i coords;
	coords.y = 0;
	for (int i = 0; i < 1; ++i)
	{
		int flower_color = GetRandom(0, 1);//decides wheter it is blue or red flower

		coords.x = GetRandom(0, 16) * 50;
		if (flower_color == 0) {
			v_flowers.push_back(std::make_shared<Flower_Red>(static_cast<int>(coords.x), 0));	//creates a flower on random x and y =0 <-top of the screan || mb add vector of coords???s
			v_coords.push_back(coords);	
		}
		else if (flower_color == 1) {
			v_flowers.push_back(std::make_shared<Flower_Blue>(static_cast<int>(coords.x), 0));
			v_coords.push_back(coords);
		}
		else throw std::invalid_argument("Error within creating flowers, |generateFlowerPosition|"); 
	}
	setTexture();
}

void FlowerManager::draw(sf::RenderTarget& obcject, sf::RenderStates states)const 
{
	for (auto& sprite : v_flowers)
		obcject.draw(*sprite, states);
}


void FlowerManager::setTexture()
{
	for (auto& base_ptr : v_flowers)
	{
		auto down_cast_red = std::dynamic_pointer_cast<Flower_Red> (base_ptr);
		auto down_cast_blue = std::dynamic_pointer_cast<Flower_Blue>(base_ptr);
		if (down_cast_red != nullptr)
			base_ptr->setTexture(this->t_red_flower);
		
		if (down_cast_blue != nullptr)
			base_ptr->setTexture(this->t_blue_flower);
		else continue;
	}
}

void FlowerManager::getTexture(sf::Texture& flower_texture_red, sf::Texture flower_texture_blue)
{
	this->t_red_flower = flower_texture_red;
	this->t_blue_flower = flower_texture_blue;
}

void FlowerManager::getPositionOfAllFlowers()
{
	sf::Vector2f coords;
	sf::Vector2i coords_i;

	v_coords.clear();
	for (auto& sprite: v_flowers)
	{
		coords = sprite->getPosition();
		coords_i.x = (int)coords.x;
		coords_i.y = (int)coords.y;
		v_coords.push_back(coords_i);
	}
}


void FlowerManager::dropFlower()
{
	for (auto& sprite : v_flowers)
		sprite->setPos();
}

void FlowerManager::checkAxis_y()
{
	sf::Vector2f tmp;
	auto toErase = std::remove_if(v_flowers.begin(), v_flowers.end(),
		[&tmp](std::shared_ptr<Flower> flower){
		tmp = flower->getPosition();
		 
		return tmp.y >= 800;
	});

	v_flowers.erase(toErase, v_flowers.end());
}


void FlowerManager::checkCol(sf::Sprite& p_sprite, int& score)
{
	auto toErase = std::remove_if(v_flowers.begin(), v_flowers.end(),
		[&p_sprite, &score](std::shared_ptr<Flower>flower) {
		bool tmp = p_sprite.getGlobalBounds().intersects((flower->getSprite()).getGlobalBounds());
		if (tmp)score+=flower->getValue();
		return tmp == true;
	});
	v_flowers.erase(toErase, v_flowers.end());

	std::cout << score << std::endl;
}


int FlowerManager::subtractHeart()
{
	int count = 0;
	getPositionOfAllFlowers();
	for (auto& cord : v_coords)
	{
		if (cord.y >= 800)count++;
	}
	checkAxis_y();
	return count;
}


FlowerManager::~FlowerManager()
{
	for (size_t i = 0; i < v_flowers.size(); ++i)
	{
		v_flowers.erase(v_flowers.begin() + i);
		--i;
	}
	v_flowers.clear();
	v_coords.clear();
	
}
#include "Game.h"


Game::Game() :
	sf_game_window(sf::VideoMode(800, 800), "Fcatch"),
	start(false),
	help(false),
	exit(false)	
{
	std::cout << "Game";
}


const bool Game::getInfo()
{
	return this->restart;
}

bool Game::loop()
{
	while (restart)
	{
		main_loop();
	}
	return true;
}

bool Game::menu(Hud& hud)
{
	while (sf_game_window.isOpen())
	{

		sf::Event event;
		while (sf_game_window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
			{
				sf_game_window.close();
				restart = false;
			}

			if (event.type == sf::Event::MouseButtonPressed)//tmp fun to help debug
			{
				sf::Vector2i soure_vector2i = sf::Mouse::getPosition(sf_game_window);
				std::cout << "\n" << soure_vector2i.x << " " << soure_vector2i.y << "     x,y of source ";
				if (soure_vector2i.x >= 280 && soure_vector2i.x <= 460)
				{
				
						if (soure_vector2i.y >= 265 && soure_vector2i.y <= 320) { //okomentowac co jest co

							hud.changeBG();
							return true;
						}
						if (soure_vector2i.y >= 405 && soure_vector2i.y <= 465)
						{
							hud.changeBG_help();
							help = help_Phase(hud);
						}
						if (soure_vector2i.y >= 555 && soure_vector2i.y <= 600)
						{
							sf_game_window.close();
						}
					}
				}
				
				
			
		}

		
		sf_game_window.clear(sf::Color::White);
		sf_game_window.draw(hud);
		sf_game_window.display();

	}
	return true;
}


bool Game::help_Phase(Hud& hud)
{
	start = false;

	while (sf_game_window.isOpen())
	{

		sf::Event event;
		while (sf_game_window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed) {
				sf_game_window.close();
				restart = false;
			}


			if (event.type == sf::Event::MouseButtonPressed)//tmp fun to help debug
			{
				sf::Vector2i soure_vector2i = sf::Mouse::getPosition(sf_game_window);
				std::cout << "\n" << soure_vector2i.x << " " << soure_vector2i.y << "     x,y of source ";
						
				if (soure_vector2i.x >= 690 && soure_vector2i.x <= 760 && soure_vector2i.y >= 165 && soure_vector2i.y <= 222)
				{
					hud.changeBG_menu();
					start = true;
					break;
				}
			}
		}

		if (start == true) break;
		sf_game_window.clear(sf::Color::White);
		sf_game_window.draw(hud);
		sf_game_window.display();

	}
	return true;
}

bool Game::main_loop()
{
	Hud hud;
	
	FlowerManager flower_manager;
	Player player;
	
		menu_p = menu(hud);

		flower_manager.getPositionOfAllFlowers();

		float timer = 0, timer2 = 0, delay = 1.5;
		float timerTest = 0;
		sf::Clock clock;

		while (sf_game_window.isOpen())
		{

			float time = clock.getElapsedTime().asSeconds();
			clock.restart();

			timer += time;
			timer2 += time;
			//timerTest += time;

			sf::Event event;
			while (sf_game_window.pollEvent(event))
			{
				if (event.type == sf::Event::Closed) {
					restart = false;
					sf_game_window.close();
					flower_manager.~FlowerManager();
					mainloop = false;
					break;
				}
				if (event.type == sf::Event::KeyPressed)
				{
					player.move(event.key.code);
				}

				if (event.type == sf::Event::MouseButtonPressed)//tmp fun to help debug
				{
					sf::Vector2i soure_vector2i = sf::Mouse::getPosition(sf_game_window);
					std::cout << "\n" << soure_vector2i.x << " " << soure_vector2i.y << "     x,y of source ";
				}
			}
			if (mainloop == false)break;
			if (timer2 > delay * 1.5) //generates new flowers
			{
				flower_manager.generateFlowerPosition();
				flower_manager.getPositionOfAllFlowers();
				timer2 = 0;
			}

			if (timer > delay) //updates score'
			{
				update(flower_manager, player, timer, hud);
				timer = 0;
			}

			if (heart < 1) {//to develop
				ending(hud);
				score = 0;
				heart = 3;
				break;
			}

			sf_game_window.clear(sf::Color::White);
			sf_game_window.draw(hud);
			sf_game_window.draw(flower_manager);
			sf_game_window.draw(player);
			sf_game_window.display();

		
	}

	std::cout << "Game over";
	return true;
}


void Game::update(FlowerManager& flower_manager, Player& player, float& timer, Hud& hud)
{
	flower_manager.dropFlower();
	flower_manager.getPositionOfAllFlowers();
	sf::Sprite s = player.getSprite();
	flower_manager.checkCol(s, score);
	heart = heart - flower_manager.subtractHeart();//later move that to player method
	std::cout << "Life left: " << heart << std::endl;
	hud.updateScore(this->score, this->heart);
	
	timer = 0;
}

void Game::ending(Hud& hud)
{
	exit = false;
	hud.changeBG_ending();
	hud.setEnding(true);
	while (sf_game_window.isOpen())
	{

		sf::Event event;
		while (sf_game_window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed) {
				sf_game_window.close();
				restart = false;
			}


			if (event.type == sf::Event::MouseButtonPressed)//tmp fun to help debug
			{
				sf::Vector2i soure_vector2i = sf::Mouse::getPosition(sf_game_window);
				std::cout << "\n" << soure_vector2i.x << " " << soure_vector2i.y << "     x,y of source ";

				
				if (soure_vector2i.x >= 120 && soure_vector2i.x <= 495)
				{
					if (soure_vector2i.y >= 370 && soure_vector2i.y <= 455) {
						hud.changeBG_menu();
						hud.setEnding(false);
						exit = true;

						break;
					}
					if (soure_vector2i.y >= 513 && soure_vector2i.y <= 587)
					{
						sf_game_window.close();
						
					}
				}



			}
		}

		if (exit == true) break;
		sf_game_window.clear(sf::Color::White);
		sf_game_window.draw(hud);
		sf_game_window.display();

	}

}
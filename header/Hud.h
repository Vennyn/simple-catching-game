#pragma once
#include "FlowerManager.h"

class Hud: public sf::Drawable
{
public:
	Hud();
	~Hud() {};
	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
	void updateScore(int score, int health);
	void loadTexture();
	void changeBG();
	void changeBG_help();
	void changeBG_menu();
	void changeBG_ending();
	void setEnding(bool decision);

private:

	bool menu_phase = true;
	bool ending = false;
	int health;
	int score;

	sf::Texture t_heart;
	sf::Texture t_backgroud;
	sf::Texture t_menu;
	sf::Texture t_help;
	sf::Texture t_ending;

	sf::Sprite s_background;
	sf::Sprite s_heart;
	sf::Sprite s_heart2;
	sf::Sprite s_heart3;

	sf::Font font;
};
#pragma once
#include "FlowerManager.h"
#include "Player.h"
#include "Hud.h"
#include <time.h>

class Game
{
public:
	Game();
	~Game() { std::cout << "delete g1"; };
	bool menu(Hud& hud);
	bool main_loop();
	bool help_Phase(Hud& hud);
	void ending(Hud& hud);
	void update(FlowerManager& flower_manager, Player& player, float& timer, Hud& hud);
	const bool getInfo();
	bool loop();

private:
	bool start, help, exit, mainloop=true, restart=true;
	sf::RenderWindow sf_game_window;
	int score;
	int heart = 3;
	bool menu_p;
};
#pragma once
#include <vector>
#include <random>
#include <iostream>
#include "Flower.h"
#include "Flower_Red.h"
#include "Flower_Blue.h"
class FlowerManager:	public sf::Drawable
{
public:
	FlowerManager();
	~FlowerManager();
	void generateFlowerPosition();
	void draw(sf::RenderTarget& obcject, sf::RenderStates states)const override;
	void setTexture();
	void getTexture(sf::Texture& flower_texture_red, sf::Texture flower_texture_blue);
	int GetRandom(int x, int y);
	void loadTexture();
	void getPositionOfAllFlowers();
	void checkAxis_y();
	void dropFlower();
	void checkCol(sf::Sprite& sprite, int& score);
	int subtractHeart();
	
private:
	std::vector<std::shared_ptr<Flower>> v_flowers; //need to add one more vec to buffor data to drop 1 flower per gameloop i guess
	std::vector<sf::Vector2i> v_coords;

	sf::Texture t_red_flower;
	sf::Texture t_blue_flower;
	
	//std::vector<std::shared_ptr<Flower>> v_colision;
	//std::vector<std::shared_ptr<Flower>> v_flowers_final; //need to add one more vec to buffor data to drop 1 flower per gameloop i guess or mb delay the first vec in time of drwaing
};